//# Tic-Tac-Toe
#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
char a[3][3],ch1,ch2;
    int i,j;
    void ask_players();
    void show_matrix();
    void player1();
    void player2();
    void check_winner();
    void over();

int main()
{
	ask_players();
	show_matrix();
	player1();
	player2();
	player1();
	player2();
	player1();
	check_winner();
	player2();
	check_winner();
    player1();
	check_winner();
	player2();
	check_winner();
    player1();
	check_winner();
	over();
	return 0;
}
void over()
{
	for(i=0;i<3;i++)
		for(j=0;j<3;j++)
		{
			if(a[i][j]== 'x' || 'o')
				printf("\n\tGAME OVER ........");
			exit(0);
		}
}
void check_winner()
{

	if(a[0][0] == 'x' && a[0][1] == 'x' && a[0][2] == 'x' || a[0][0] =='x' && a[1][0] =='x' && a[2][0]=='x'  || a[2][0] =='x' && a[2][1] == 'x' &&a[2][2]=='x' || a[2][2] =='x' && a[1][2] =='x' && a[0][2] =='x' || a[0][0] =='x' && a[1][1] =='x' && a[2][2] =='x' || a[2][0] =='x' && a[1][1] =='x' && a[0][2]=='x'|| a[1][0] =='x' && a[1][1] =='x' && a[1][2]=='x' ||  a[0][1] =='x' && a[1][1] =='x' && a[2][1]=='x' )
     {
     	printf("\nCongratulations PLayer1 :) You are the winner. \n Player2, better luck next time");
     	exit(0);
     }
    
    if(a[0][0] == 'o' && a[0][1] == 'o' && a[0][2] =='o'  || a[0][0] == 'o' &&a[1][0] == 'o' &&a[2][0] =='o' || a[2][0] == 'o' &&a[2][1] =='o' && a[2][2] =='o' || a[2][2] == 'o' &&a[1][2] == 'o' &&a[0][2] =='o' || a[0][0] =='o' && a[1][1] =='o' && a[2][2] =='o' || a[2][0] =='o' && a[1][1] == 'o' &&a[0][2] =='o' || a[1][0] =='o' && a[1][1] =='o' && a[1][2]=='o' || a[0][1] =='o' && a[1][1] =='o' && a[2][1]=='o')
     {
     	printf("\nCongratulations PLayer2 :) You are the winner. \n Player1, better luck next time");
        exit(0);

    }
}


void player1()
{
	printf("\nPLayer1 make your move by entering the cell number. And make sure you seperate cell numbers with a space:");
	scanf("%d%d",&i,&j);
	a[i][j]=ch1;
	for(i=0;i<3;i++)
		for(j=0;j<3;j++)
		{
			printf("%c\t",a[i][j]);
			if(i==0 && j==2 || i==1 && j==2)
				printf("\n");
		}
		
}

void player2()
{
    printf("\nPlayer2 make your move be entering the cell number. And make sure you seperate cell numbers with a space:");
    scanf("%d%d",&i,&j);
    a[i][j]=ch2;
    for(i=0;i<3;i++)
		for(j=0;j<3;j++)
		{
			printf("%c\t",a[i][j]);
			if(i==0 && j==2 || i==1 && j==2)
				printf("\n");
		}
		
}

	
void show_matrix()
{
	printf("\nThis is your play area. Note the index number of the cells. You need to enter the cell number while playing. Have a good game :)\n");
	for(i=0;i<3;i++)
		for(j=0;j<3;j++)
		{
			printf("%d%d\t",i,j);
			if(i==0 && j==2 || i==1 && j==2)
				printf("\n");
		}

}

void ask_players()
{
	printf("\nPLayer1 Choose your character 'x' or 'o' :");
    scanf("%c",&ch1);
    if(ch1 =='x' || ch1 == 'X')
    	ch2 = 'o';
    else
    	ch2 = 'x';
}



